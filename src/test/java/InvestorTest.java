import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertThat;


import org.junit.Test;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Consumer;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import com.citi.trading.*;
import com.citi.trading.Trade.Result;

public class InvestorTest {
	private MockMarket mock;
	private Investor testInvestor;
	
	private class MockMarket implements OrderPlacer{

		public synchronized void placeOrder(Trade order, Consumer<Trade> callback) {
			if (order.getSize()>150) {
				order.setResult(Result.PARTIALLY_FILLED);
				order.setSize(150);
			}
			if (order.getStock() == "Ibad") {
				order.setResult(Result.REJECTED);
			}
			callback.accept(order);
		}
	}
	
	@Before
	public void prepare() {
		mock = new MockMarket();	
	}
	
	//Can do the same thing for getPortfolio, skipping it to get to stretch goals
	@Test
	public void testGetCash() {
		testInvestor = new Investor(6000, mock);
		assertThat(testInvestor.getCash(),closeTo(6000, 0.0000));
	}
	
	@Test
	public void testBuy() {
		testInvestor = new Investor(6000, mock);
		testInvestor.buy("MRK", 100, 60);
		assertThat(testInvestor.getCash(),closeTo(0, 0.0000));
		assertThat(testInvestor.getPortfolio(), hasEntry("MRK", 100));
		
	}
	
	@Test
	public void testPartialBuy() {
		testInvestor = new Investor(2000, mock);
		testInvestor.buy("MRK", 200, 10);
		assertThat(testInvestor.getCash(),closeTo(500, 0.0000));
		assertThat(testInvestor.getPortfolio(), hasEntry("MRK", 150));
	}
	
	@Test
	public void testRejectedBuy() {
		testInvestor = new Investor(2000, mock);
		testInvestor.buy("Ibad", 200, 10);
		assertThat(testInvestor.getCash(),closeTo(2000, 0.0000));
		assertThat(testInvestor.getPortfolio(), not(hasEntry("Ibad", 200)));
	}
	
	@Test
	public void testSell() {
		Map<String,Integer> portfolio = new HashMap<String, Integer>();
		portfolio.put("GOOG", 100);
		testInvestor = new Investor( portfolio, 0, mock);
		
		testInvestor.sell("GOOG", 100, 100);
		
		assertThat(testInvestor.getCash(),closeTo(10000, 0.0000));
		assertThat(testInvestor.getPortfolio(), not(hasEntry("GOOG", 100)));
		
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void testBuyIllegalArgument() {
		testInvestor = new Investor(6000, mock);
		testInvestor.buy("GOOG", -100, 10);

	}
	@Test(expected = IllegalArgumentException.class)
	public void testCashIllegalArgument() {
		testInvestor = new Investor(-6000, mock);
	}
}
